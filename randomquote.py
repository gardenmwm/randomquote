from flask_api import FlaskAPI
import random, json

app = FlaskAPI(__name__)

quotes = [{'quote':"Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.", 'author':'Albert Einstein'},
          {'quote':"Insanity is doing the same thing, over and over again, but expecting different results.",'author':'Narcotics Anonymous'},
          {'quote':"Get your facts first, then you can distort them as you please", "author":"Mark Twain"} 
         ]


@app.route('/getquote/')
def getquote():
    return random.choice(quotes)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
else:
    app.run()